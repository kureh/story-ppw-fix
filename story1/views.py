from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from .forms import MatkulForm
from .models import MataKuliah

response = {}

# Create your views here.
def index(request):
    return render(request, 'index.html')

def story1(request):
    return render(request, 'story1.html')

def course(request):
    matakuliah = MataKuliah.objects.all
    return render(request, 'form_course.html', {'matakuliah' : matakuliah})

def addcourse(request):
	form = MatkulForm(request.POST or None)
	if (form.is_valid and request.method == 'POST'):
		form.save()
		return redirect('/course')

	return render(request, 'form.html', {'form' : form})

def deletecourse(request,pk):
	matakuliah = MataKuliah.objects.get(id=pk)
	matakuliah.delete()
	return HttpResponseRedirect('/course')

		
