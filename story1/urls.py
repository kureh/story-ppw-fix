from django.urls import path

from . import views

app_name = 'story1'

urlpatterns = [
    path('', views.index, name='index'),
    path('story1/', views.story1, name='story1'),
    path('home/', views.index, name='home'),
    path('course/', views.course, name='course'),
    path('addcourse/', views.addcourse, name='addcourse'),
    path('deletecourse/<str:pk>/', views.deletecourse, name='deletecourse')
]   