from django import forms
from .models import MataKuliah

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = [
            'nama_mata_kuliah',
            'dosen_mata_kuliah',
            'sks_mata_kuliah',
            'deskripsi_mata_kuliah',
            'semester_mata_kuliah',
            'ruang_mata_kuliah',
        ]

    matkul_attrs  = {
    'type' : 'text',
    'placeholder' : 'Nama Mata Kuliah'
    }

    dosen_attrs  = {
    'type' : 'text',
    'placeholder' : 'Nama Dosen'
    }

    sks_attrs  = {
    'type' : 'text',
    'placeholder' : 'Berapa SKS'
    }

    deskripsi_attrs  = {
    'type' : 'text',
    'placeholder' : 'Deskripsi'
    }

    semester_attrs  = {
    'type' : 'text',
    'placeholder' : 'Semester'
    }

    ruang_attrs  = {
    'type' : 'text',
    'placeholder' : 'Ruang Kuliah'
    }

    nama_mata_kuliah = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs=matkul_attrs))
    dosen_mata_kuliah = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=dosen_attrs))
    sks_mata_kuliah = forms.CharField(label='', required=True, max_length=3, widget=forms.TextInput(attrs=sks_attrs))
    deskripsi_mata_kuliah = forms.CharField(label='', required=True, max_length=200, widget=forms.TextInput(attrs=deskripsi_attrs))
    semester_mata_kuliah = forms.CharField(label='', required=True, max_length=10, widget=forms.TextInput(attrs=semester_attrs))
    ruang_mata_kuliah = forms.CharField(label='', required=True, max_length=10, widget=forms.TextInput(attrs=ruang_attrs))