from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    nama_mata_kuliah = models.CharField(max_length=100)
    dosen_mata_kuliah = models.CharField(max_length=30)
    sks_mata_kuliah = models.CharField(max_length=3)
    deskripsi_mata_kuliah = models.CharField(max_length=200)
    semester_mata_kuliah = models.CharField(max_length=10)
    ruang_mata_kuliah = models.CharField(max_length=10)


class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=30)


class Orang(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    nama_orang = models.CharField(max_length=50)