from django.test import TestCase, Client
from django.urls import resolve
from .views import book
from django.http import HttpRequest

# Create your tests here.
class Test(TestCase):
    def test_app_url_ada(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page(self):
        self.assertIsNotNone(book)
