from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage
from django.http import HttpRequest
from django.contrib.auth.models import User


# Create your tests here.
class Test(TestCase):
    def test_app_url_ada(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_ada(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_ada(self):
        response = Client().get('/story9/register/')
        self.assertEqual(response.status_code, 200)

    def test_app_ada(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, homepage)

    def test_template_landpage(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_template_login(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_template_register(self):
        response = Client().get('/story9/register/')
        self.assertTemplateUsed(response, 'register.html')

    def test_landing_page(self):
        self.assertIsNotNone(homepage)

    def test_register_berhasil(self):
        counter = User.objects.all().count()
        context = {
            'username' : 'labib',
            'password1' : 'password',
            'password2' : 'password'
        }
        response = self.client.post('/story9/register/', data = context)
        self.assertEqual(User.objects.all().count(), counter)

    def test_login_berhasil(self):
        context = {
            'username' : 'labib',
            'password' : 'password'
        }
        response = self.client.post('/story9/login/', data = context)
        self.assertEqual(response.status_code, 200)

    def test_logout_berhasil(self):
        self.client.login(username = 'labib', password = 'password')
        response = self.client.get('/story9/logout/')
        self.assertEqual(response.status_code, 302)
